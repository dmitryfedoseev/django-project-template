# setuptools-friendly Django project template. #

### What is this repository for? ###

This is not Django project nor Python package, this is Django project template.
Do not be confused - files are not Python scripts but Django templates.

"django-admin startapp" command creates new projects using project templates,
most common one is a part of Django.

The goal of this template is compatibility with setuptools. Projects based
on this template may be installed using pip or easy_install into multiple
environmens or used multiple times on same host using different configurations.

This template improves tuning of Django projects. Commonly setting.py used
as main configuration file. Being installed in production environment
into /usr or /usr/local this file may have been restricted to read-only access
mode. Moreover, once installed this package may be used multiple times within
different instances on same host. That's why part of configuration splited
out of settings.py and placed into home folder of instance
owner (~/.config/<myproject>/settings.json file,
[consult PyXDG documentation for details](http://pyxdg.readthedocs.io/en/latest/basedirectory.html#configuration-directories)).

### How do I get set up? ###

#### In development environment ####

Clone content of repository to local computer; project_template folder contains
desired Django templates.

Create new Django project using cloned template:
"django-admin startproject --template <path to project_template folder> myproject .".
This will create myproject folder in current folder, myproject/setup.py file
and myproject/src/ filder and some files inside.

Step into myproject folder. Contents of myproject folder is source code of
trivial Python package.

(It's time to activate virtualenv if used.)

Install package locally in editable mode: 'pip install -e .' (the dot means
current directory). Installing of package allow using myproject-admin command.
This command is similar to django-admin but uses settings of myproject.
In fact, it is wrapper of manage.py command. The only difference is you do not
need to change directory every time you are going to use manage.py.

Create settings.json file. The absilute minimum is definition of SECRET_KEY, for example:
{ "SECRET_KEY": "generate new value for every new instance using uuidgen utility or anything else" }.

At this point you can test sources and installation: run "myproject-admin shell".

Add applications to project using "myproject-admin startapp app1". There is
no template of application currently, so default one used.

Initialize version control in myproject folder, commit and push initial changes.

#### In targeted environment ###

Install package from VSC into target environment using 'pip install <repo URL>.
Create settings.json for target environment.
