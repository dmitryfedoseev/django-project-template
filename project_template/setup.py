# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='{{ project_name }}',
    version='0.0.0',
    author='put your name here',
    author_email='put your email adddress here',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    include_package_data=True,
    package_data={'': ['static/*', 'templates/*']},
    namespace_packages=['{{ project_name }}'],
    install_requires=[
        'django',
        'pyxdg',
    ],
    entry_points={
        'console_scripts': ['{{ project_name }}-admin={{ project_name }}.core.manage:main'],
    },
)
